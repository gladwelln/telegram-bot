@extends('layouts.dashboard')

@section('content')
<div class="row m-2">
    <div class="col-md-12">
        <p class="text-info">Provide your default bot settings:
            <a class="float-right btn btn-success btn-sm" href="{{ route('set-hook') }}">Set Webhook</a>
        </p>
        <div class="shadow-lg mt-2 p-4 bg-white rounded">
            <form id="settings-form">
                <div class="form-group row">
                    <label for="telegram_username" class="col-sm-2 col-form-label">Telegram username</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="telegram_namusere" name="telegram_username"
                            aria-describedby="telegramUsernameInfo" value="{{ $settings['telegram_username'] ?? '' }}">
                        <small id="telegramUsernameInfo" class="form-text text-muted">
                            We'll never share your telegram username with anyone else.
                        </small>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="default_currency" class="col-sm-2 col-form-label">Default currency</label>
                    <div class="col-sm-10">
                        <select class="form-control" id="default_currency" name="default_currency"
                            aria-describedby="defaultCurrencyInfo">
                            @foreach ($currencies as $currency)
                            <option value="{{ $currency->name }}"
                                {{ (isset($settings['default_currency']) && $settings['default_currency'] == $currency->name) ? 'selected' : '' }}>
                                {{ $currency->name }}
                            </option>
                            @endforeach
                        </select>
                        <small id="defaultCurrencyInfo" class="form-text text-muted">
                            This will be used when you execute telegram bot command "<span
                                class="text-info">/getBTCEquivalent</span>" without
                            parameters.
                        </small>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="default_amount" class="col-sm-2 col-form-label">Default amount</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="default_amount" name="default_amount"
                            aria-describedby="defaultAmountInfo" value="{{ $settings['default_amount'] ?? '' }}">
                        <small id="defaultAmountInfo" class="form-text text-muted">
                            This will be used when you execute telegram bot command "<span
                                class="text-info">/getBTCEquivalent</span>" without
                            parameters.
                        </small>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="col-md-12 d-flex justify-content-center">
        <button class="btn btn-secondary btn-block w-50 mt-4 float-right" onclick="saveSettings()">Save</button>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('js/custom.js') }}"></script>
<script>
    var telegramLinked = parseInt("{{ $user->telegram_linked }}");
    var telegramID = "{{ $user->telegram_id }}";
    var message = (telegramLinked) ? `Your telegram ID is ${telegramID}` : 'Open your telegram, search for @dim_notas_bot and click on /start command';
    var delay = alertify.get('notifier','delay');
    alertify.set('notifier','delay', 120);
    alertify.message(message);
    alertify.set('notifier','delay', delay);
</script>
@endpush
