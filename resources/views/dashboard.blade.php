@extends('layouts.dashboard')

@section('content')
<div class="row m-2">
    <div class="col-md-12">
        <p class="text-info">Transaction History</p>
    </div>
    <div class="col-md-12">
        <table class="table table-striped table-hover table-drk rounded">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">Currency Name</th>
                    <th scope="col">Amount</th>
                    <th scope="col">Total</th>
                    <th scope="col">Rate</th>
                    <th scope="col">Created At</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($history as $data)
                <tr>
                    <td>{{ $data->currency->name }}</td>
                    <td>{{ $data->amount }}</td>
                    <td>{{ $data->total }}</td>
                    <td>{{ $data->rate }}</td>
                    <td>{{ $data->created_at }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{ $history->links() }}
    </div>
</div>
@endsection
