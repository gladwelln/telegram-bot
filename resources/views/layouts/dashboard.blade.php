@extends('layouts.app')
@section('main-content')
<div class="container mt-5">
    @yield('content')
</div>
@endsection
