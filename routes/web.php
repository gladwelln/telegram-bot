<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true]);

Route::group(['middleware' => ['verified', 'auth']], function () {
    Route::get('/', 'DashboardController@index');
    Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

    Route::group(['prefix' => 'currencies'], function () {
        Route::get('/', 'CurrenciesController@index')->name('currencies.index');
        Route::get('/sync', 'CurrenciesController@sync')->name('currencies.sync');
        Route::delete('/{currencyId}/remove', 'CurrenciesController@remove')->name('currencies.remove');
    });

    Route::get('/bot-config', 'SettingsController@index')->name('bot-config');
    Route::post('/save-settings', 'SettingsController@saveSettings');
    Route::get('set-hook', 'BotController@setWebHook')->name('set-hook');
});

Route::post(env('TELEGRAM_BOT_TOKEN') . '/webhook', 'BotController@handleRequest');
