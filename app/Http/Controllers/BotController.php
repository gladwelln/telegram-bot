<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Telegram\Bot\Api;
use GuzzleHttp\Client;
use App\Setting;
use Telegram\Bot\Exceptions\TelegramSDKException;
use Log;
use App\User;
use App\History;
use App\Currency;

class BotController extends Controller
{
    protected $telegram;
    protected $username;
    protected $chatId;
    protected $command;
    protected $userId;

    public function __construct()
    {
        $this->telegram = new Api(env('TELEGRAM_BOT_TOKEN'));
    }

    /**
     * The method to set a url for receiving incoming bot updates.
     */
    public function setWebHook()
    {
        try {
            $this->telegram->setWebhook(['url' => env('TELEGRAM_WEBHOOK_URL')]);
            return redirect()->back()->with('success', 'Webhook successfully set!');
        } catch (TelegramSDKException $e) {
            return redirect()->back()->with('success', 'Opps!! Something went wrong, please try again!');
        }
    }

    /**
     * Handles incoming messages
     */
    public function handleRequest(Request $request)
    {
        if (!$request->message) {
            return;
        }

        $this->chatId = $request->message['chat']['id'];
        $this->command = $request->message['text'];

        // If telegram username not set
        if (!isset($request->message['from']['username'])) {
            return $this->sendMessage("Please set your telegram username and link your account at " . env('APP_URL') . " to proceed");
        }

        $this->username = $request->message['from']['username'];

        $user = $this->getUser();
        if (!$user) {
            return $this->sendMessage("Hi {$this->username}, please link your account at " . env('APP_URL') . " to proceed");
        }

        $this->userId = $user->user_id;

        if ($this->command === '/start') {
            $this->linkAccount();
            return $this->showCommands("Welcome {$this->username}, select a command to proceed");
        } elseif ($this->command === '/getUserID') {
            return $this->getUserID();
        } else {
            $commandArr = explode(' ', $this->command);
            if (isset($commandArr[0]) && $commandArr[0] === '/getBTCEquivalent') {
                return $this->getBTCEquivalent($commandArr);
            } else {
                return $this->showCommands('Invalid command, please select a command below:');
            }
        }
    }

    /**
     * Shows a list of supported commands
     *
     * @param tring $title
     */
    public function showCommands($title)
    {
        $message  = "<b>$title</b>" . chr(10);
        $message .= '/getBTCEquivalent' . chr(10);
        $message .= '/getUserID' . chr(10);

        $this->sendMessage($message);
    }

    /**
     * Gets telegram user details
     */
    private function getUser()
    {
        return Setting::where('setting_key', 'telegram_username')->where('setting_value', $this->username)->first();
    }

    /**
     * Gets telegram user settings
     */
    private function getUserSettings()
    {
        return Setting::where('user_id', $this->userId)->get();
    }

    /**
     * Links telegram account to the user
     */
    private function linkAccount()
    {
        $user = User::find($this->userId);
        if (!$user->telegram_linked) {
            $user->telegram_linked = 1;
            $user->telegram_id = $this->chatId;
            $user->save();
        }
    }

    /**
     * Calculates a bitcoin equivalent of a given currency
     *
     * @param array $commandArr
     */
    private function getBTCEquivalent($commandArr)
    {
        if (isset($commandArr[1]) && isset($commandArr[2])) {
            $amount = $commandArr[1];
            $currency = strtoupper($commandArr[2]);
        } else {
            $settings = $this->getUserSettings();
            $settings = ($settings) ? array_pluck($settings, 'setting_value', 'setting_key') : [];
            if (empty($settings) || (!isset($settings['default_amount']) || !isset($settings['default_currency']))) {
                return $this->sendMessage("Please set your default currency settings at " . env('APP_URL') . ".");
            }

            $amount = $settings['default_amount'];
            $currency = $settings['default_currency'];
        }

        // Get price from CoinDesk
        $client = new Client(['base_uri' => env('COINDESK_API')]);
        $res = $client->request('GET', "currentprice/{$currency}.json", ['http_errors' => false]);
        if ($res->getStatusCode() != 200) {
            return $this->sendMessage("Invalid currency, please try again");
        }

        $body = $res->getBody()->getContents();
        $results = json_decode($body, true);
        $results = $results['bpi'][$currency];

        $rate = number_format((float) $results['rate_float'], 2, '.', '');
        $total = number_format((1 * $amount / $rate), 2);

        $message = "$amount $currency is $total BTC ({$rate} $currency - 1 BTC)";

        $this->logHistory($currency, $rate, $amount, $total);

        $this->sendMessage($message);
    }

    private function logHistory($currencyName, $rate, $amount, $total)
    {
        $currency = Currency::where('name', $currencyName)->first();
        History::create([
            'user_id' => $this->userId,
            'currency_id' => $currency->id,
            'rate' => $rate,
            'amount' => $amount,
            'total' => $total
        ]);
    }

    /**
     * Gets application userId linked with telegram user.
     */
    private function getUserID()
    {
        return $this->sendMessage("Your user ID is {$this->userId}");
    }

    /**
     * Sends messages to telegram
     */
    protected function sendMessage($message)
    {
        $data = [
            'parse_mode' => 'html',
            'chat_id' => $this->chatId,
            'text' => $message,
        ];

        $this->telegram->sendMessage($data);
    }
}
