<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    public $timestamps = false;
    public static function findOrCreate($settingKey, $user_id)
    {
        $obj = static::where('user_id', $user_id)->where('setting_key', $settingKey)->first();
        return $obj ?: new static;
    }
}
