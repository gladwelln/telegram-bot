@extends('layouts.app')
@section('main-content')
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h3 class="text-center mb-4 text-info">@yield('heading')</h3>
            <div class="shadow-lg p-5 m-2 bg-white rounded">
                @yield('content')
            </div>
        </div>
    </div>
</div>
@endsection
