<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Currency;
use App\Setting;

class SettingsController extends Controller
{
    public function index()
    {
        $currencies = Currency::where('active', 1)->get();
        $user = auth()->user();
        $settings = $user->settings->pluck('setting_value', 'setting_key')->toArray();

        return view('settings.index', compact('currencies', 'settings', 'user'));
    }

    public function saveSettings(Request $request)
    {
        $user_id = $request->user()->id;
        $settings = $request->settings;
        foreach ($settings as $key => $val) {
            $setting = Setting::findOrCreate($key, $user_id);
            $setting->user_id = $user_id;
            $setting->setting_key = $key;
            $setting->setting_value = $val;
            $setting->save();
        }

        return response()->json(['status' => 'ok']);
    }
}
