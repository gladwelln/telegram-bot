<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\History;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    { }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $history = auth()->user()->history()->with('currency')->paginate(15);
        return view('dashboard', compact('history'));
    }
}
