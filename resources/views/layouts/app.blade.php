<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/alertify.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/alertify-bootstrap.min.css') }}" rel="stylesheet">
    @stack('styles')
</head>

<body>
    <div id="app">
        @include('layouts.common.nav')

        <main class="py-4">
            @yield('main-content')
        </main>
        <p class="text-info text-center my-3">
            Powered by <a href="https://www.coindesk.com/price/bitcoin" target="_blank">CoinDesk</a>
        </p>
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/alertify.min.js') }}"></script>
    <script type="text/javascript">
        var baseUrl = "{{ url('/') }}";
        var _token = "{{ csrf_token() }}";
    </script>
    @stack('scripts')

    @include('layouts.common.flash-message')
</body>

</html>
