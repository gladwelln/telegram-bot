$.extend({
    doAJAX: function (url, data, type, callback) {

        data["_token"] = _token;

        return $.ajax({
            type: type,
            url: url,
            data: data,
            dataType: "json",
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alertify.error(`Error Code: ${XMLHttpRequest.status}  : ${XMLHttpRequest.statusText}`);
            },
            success: function (data) {
                callback(data);
            }
        });
    }
});

function saveSettings() {
    let settings = {};
    let errorCount = 0;
    $('#settings-form .form-control').each(function () {
        $(this).removeClass('is-invalid');
        if (!$(this).val()) {
            $(this).addClass('is-invalid');
            errorCount++;
        } else {
            settings[$(this).attr('name')] = $(this).val();
        }
    });

    if (errorCount) {
        return false;
    }

    $.doAJAX(`${baseUrl}/save-settings`, { settings }, 'POST', (res) => {
        if (res.status === 'ok') {
            alertify.success('Settings cuccessfully saved!')
        } else {
            alertify.error('Ooops!! Something went wrong. Please try again.')
        }
    });
}

function removeCurrency(currencyId) {
    alertify.confirm('Are you sure you want to remove this currency?', 'The currency will be removed and this cannot be undone!', () => {
        $.doAJAX(`${baseUrl}/currencies/${currencyId}/remove`, {}, 'DELETE', (res) => {
            if (res.status === 'ok') {
                $(`.currency-${currencyId}`).fadeOut('slow');
                alertify.success('Settings cuccessfully removed!')
            } else {
                alertify.error('Ooops!! Something went wrong. Please try again.')
            }
        });
    }, () => {
        alertify.message("Action cancelled")
    });
}
