## About Telegram Bot

Telegram bot which accesses an external api to get data and which outputs this data on request to a telegram channel.

## Installation
- git clone https://bitbucket.org/gladwelln/telegram-bot.git
- cp .env.example .env
- composer install

Open .env file and provide values to the following:

- DB Credentials
- Mail Credentials
- TELEGRAM_BOT_TOKEN ([documentation](https://core.telegram.org/api))
- TELEGRAM_WEBHOOK_URL (Configure a webserver with HTTPS or [use](https://ngrok.com/docs) for a quick test)

Execute the command to migrate database tables: php artisan migrate

Access the app http://app-url and you should be presented with a login screen.

You may also access the demo [here](https://telebot.creatic.co.za)
