@extends('layouts.dashboard')

@section('content')
<div class="row m-2">
    <div class="col-md-12 mb-2">
        <a href="{{ route('currencies.sync') }}" class="btn btn-success float-right">
            CoinDesk Sync
        </a>
    </div>
    <div class="col-md-12">
        <table class="table table-striped table-hover table-drk rounded">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Currency Name</th>
                    <th scope="col">Currency Country</th>
                    <th scope="col">Active</th>
                    <th scope="col" class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($currencies as $currency)
                <tr class="currency-{{ $currency->id }}">
                    <th scope="row">{{ $currency->id }}</th>
                    <td>{{ $currency->name }}</td>
                    <td>{{ $currency->country }}</td>
                    <td>{{ $currency->active ? 'Yes' : 'No' }}</td>
                    <td class="text-center">
                        <button type="button" class="btn btn-danger btn-sm"
                            onclick="removeCurrency('{{ $currency->id }}')">
                            Delete
                        </button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{ $currencies->links() }}
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('js/custom.js') }}"></script>
@endpush
