<script type="text/javascript">
    //override defaults
    alertify.defaults.transition = "slide";
    alertify.defaults.theme.ok = "btn-sm btn btn-primary";
    alertify.defaults.theme.cancel = "btn-sm btn btn-danger";
    alertify.defaults.theme.input = "form-control";
    alertify.defaults.notifier.position = "bottom-right";

    @if ($message = Session::get('success'))
        alertify.success("{{ $message }}")
    @endif

    @if ($message = Session::get('error'))
        alertify.error("{{ $message }}")
    @endif

    @if ($message = Session::get('warning'))
        alertify.warning("{{ $message }}")
    @endif

    @if ($message = Session::get('info'))
        alertify.message("{{ $message }}")
    @endif

</script>
