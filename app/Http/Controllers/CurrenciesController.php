<?php

namespace App\Http\Controllers;

use App\Currency;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use App\History;

class CurrenciesController extends Controller
{
    public function index()
    {
        $currencies = Currency::paginate(10);
        return view('currencies.index', compact('currencies'));
    }

    public function sync()
    {
        try {
            $client = new Client(['base_uri' => env('COINDESK_API')]);
            $response = $client->request('GET', 'supported-currencies.json');
            $body = $response->getBody()->getContents();
            $records = json_decode($body, true);

            if (count($records)) {
                foreach ($records as $record) {
                    $currency = Currency::findOrCreate($record['country']);
                    $currency->name = $record['currency'];
                    $currency->country = $record['country'];
                    $currency->save();
                }
            }
        } catch (RequestException $e) {
            return back()->with('error', 'Could not sync currencies, PLease try again.');
        }

        return back()->with('success', 'Currencies successfully sync.');
    }

    public function remove($currencyId)
    {
        History::where('currency_id', $currencyId)->delete();
        Currency::where('id', $currencyId)->delete();

        return response()->json(['status' => 'ok']);
    }
}
