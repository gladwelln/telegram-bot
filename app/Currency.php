<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    public static function findOrCreate($country)
    {
        $obj = static::where('country', $country)->first();
        return $obj ?: new static;
    }
}
